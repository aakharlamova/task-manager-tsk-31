package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Optional<Task> bindTaskByProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId);

    @NotNull
    Optional<Task> unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String projectId);

}
