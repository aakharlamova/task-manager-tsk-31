package ru.kharlamova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "update-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Update info about your profile.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("[ENTER FIRST NAME:]");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final Optional<User> user = serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        System.out.println("[OK]");
    }

}
