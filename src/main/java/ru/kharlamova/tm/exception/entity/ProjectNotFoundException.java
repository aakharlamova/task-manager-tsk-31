package ru.kharlamova.tm.exception.entity;

import ru.kharlamova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found.");
    }

}
